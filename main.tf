module "vpc_config" {
  source       = "git::https://gitlab.com/terraswifft/platform/starter/vpc_details.git?ref=main"
  vpc_id       = try(var.opensearch_conf.vpc, var.vpc)
  vpc_name     = try(var.opensearch_conf.vpc_name, var.vpc_name)
  subnet_ids   = try(var.opensearch_conf.subnet_ids, var.subnet_ids)
  subnet_names = try(var.opensearch_conf.subnet_names, var.subnet_names)
}
output "test" {
  value = module.vpc_config
}
module "opensearch" {
  source                                         = "cyberlabrs/opensearch/aws"
  version                                        = "0.0.16"
  region                                         = try(var.opensearch_conf.region, var.region)
  engine_version                                 = try(var.opensearch_conf.engine_version, var.engine_version)
  name                                           = var.opensearch_conf.name
  master_user_name                               = try(var.opensearch_conf.master_user_name, var.master_user_name)
  master_password                                = try(var.opensearch_conf.master_password, var.master_password)
  master_user_arn                                = try(var.opensearch_conf.master_user_arn, var.master_user_arn)
  instance_type                                  = try(var.opensearch_conf.instance_type, var.instance_type)
  domain_endpoint_options_enforce_https          = try(var.opensearch_conf.domain_endpoint_options_enforce_https, var.domain_endpoint_options_enforce_https)
  custom_endpoint_enabled                        = try(var.opensearch_conf.custom_endpoint_enabled, var.custom_endpoint_enabled)
  custom_endpoint                                = try(var.opensearch_conf.custom_endpoint, var.custom_endpoint)
  custom_endpoint_certificate_arn                = try(var.opensearch_conf.custom_endpoint_certificate_arn, var.custom_endpoint_certificate_arn)
  volume_size                                    = try(var.opensearch_conf.volume_size, var.volume_size)
  volume_type                                    = try(var.opensearch_conf.volume_type, var.volume_type)
  access_policy                                  = jsonencode(try(var.opensearch_conf.access_policy, var.access_policy))
  tls_security_policy                            = try(var.opensearch_conf.tls_security_policy, var.tls_security_policy)
  vpc                                            = module.vpc_config.vpc_id
  subnet_ids                                     = module.vpc_config.subnet_ids
  inside_vpc                                     = try(var.opensearch_conf.inside_vpc, var.inside_vpc)
  cognito_enabled                                = try(var.opensearch_conf.cognito_enabled, var.cognito_enabled)
  allowed_cidrs                                  = try(var.opensearch_conf.allowed_cidrs, var.allowed_cidrs)
  zone_id                                        = try(var.opensearch_conf.zone_id, var.zone_id)
  advanced_security_options_enabled              = try(var.opensearch_conf.advanced_security_options_enabled, var.advanced_security_options_enabled)
  identity_pool_id                               = try(var.opensearch_conf.identity_pool_id, var.identity_pool_id)
  user_pool_id                                   = try(var.opensearch_conf.user_pool_id, var.user_pool_id)
  cognito_role_arn                               = try(var.opensearch_conf.cognito_role_arn, var.cognito_role_arn)
  implicit_create_cognito                        = try(var.opensearch_conf.implicit_create_cognito, var.implicit_create_cognito)
  internal_user_database_enabled                 = try(var.opensearch_conf.internal_user_database_enabled, var.internal_user_database_enabled)
  create_a_record                                = try(var.opensearch_conf.create_a_record, var.create_a_record)
  ebs_enabled                                    = try(var.opensearch_conf.ebs_enabled, var.ebs_enabled)
  aws_service_name_for_linked_role               = try(var.opensearch_conf.aws_service_name_for_linked_role, var.aws_service_name_for_linked_role)
  default_policy_for_fine_grained_access_control = try(var.opensearch_conf.default_policy_for_fine_grained_access_control, var.default_policy_for_fine_grained_access_control)
  advanced_options                               = try(var.opensearch_conf.advanced_options, var.advanced_options)
  iops                                           = try(var.opensearch_conf.iops, var.iops)
  throughput                                     = try(var.opensearch_conf.throughput, var.throughput)
  cluster_config                                 = try(var.opensearch_conf.cluster_config, var.cluster_config)
  encrypt_at_rest                                = try(var.opensearch_conf.encrypt_at_rest, var.encrypt_at_rest)
  log_publishing_options                         = try(var.opensearch_conf.log_publishing_options, var.log_publishing_options)
  node_to_node_encryption                        = try(var.opensearch_conf.node_to_node_encryption, var.node_to_node_encryption)
  tags                                           = try(var.opensearch_conf.tags, var.tags)
  sg_ids                                         = try(var.opensearch_conf.sg_ids, var.sg_ids)
  create_linked_role                             = try(var.opensearch_conf.create_linked_role, var.create_linked_role)
}