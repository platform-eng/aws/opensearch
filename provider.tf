provider "aws" {
  region              = var.common_config.region
  allowed_account_ids = var.common_config.allowed_account_ids
  default_tags {
    tags = var.common_config.default_tags
  }
} 