output "opensearch" {
  value     = module.opensearch
  sensitive = true
}